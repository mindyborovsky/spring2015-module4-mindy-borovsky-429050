import sys, os, re

#checks if correct number of arguments were provided, only accepts two arguments (no more no less)
if len(sys.argv) != 2:
    sys.exit("Usage: %s filename" % sys.argv[0])
 
#stores filename as second parameter passed in since need to type the python file to excecute first
filename = sys.argv[1]
 
if not os.path.exists(filename):
    sys.exit("Error: File '%s' not found" % sys.argv[1])

#opens file based on name passed in
f = open(filename)

#defines a player class to store information about a player read in from the file
class Player:

    def __init__(self, name, numBat, hits):
        self.name = name
        self.numBat = numBat
        self.hits = hits

    def __eq__(self, other):
        return self.name == other.name

    def __hash__(self):
        return hash(self.name)

    def __lt__(self, other):
        return self.average < other.average
    
    def average(self):
        self.average = float(self.hits)/self.numBat

    def printAverage(self):
        formattedAverage = format(round(self.average,3), '.3f')
        print "%s: %s" % (self.name, formattedAverage)

    def addGame(self, batsInGame, hitsInGame):
        self.numBat += batsInGame
        self.hits += hitsInGame

#storing the players in a list
players = set()

#only takes lines of appropriate format and groups them accordingly to store the name, times batted and hits
baseball_line_regex = re.compile(r"^(\b[A-Za-z]+\b \b[A-Za-z]+\b) batted (\d+) times with (\d+) hits and (\d+) runs$")

#reads in lines from files and checks if the line is a valid one to update a player's stats
for line in f:
    line = line.rstrip()
    match = baseball_line_regex.match(line)
    if match is not None:
        name = match.group(1)
        numBat = int(match.group(2))
        hits = int(match.group(3))

        currentPlayer = Player(name, numBat, hits)
        if (currentPlayer not in players):
            players.add(currentPlayer)
        else:
            for existingPlayer in players:
                if (currentPlayer == existingPlayer):
                    existingPlayer.addGame(numBat, hits)    

f.close()



for player in players:
    player.average()

#converts set of players to list for easy sorting
playerList = sorted(players)
playerList.reverse()

for player in playerList:
    player.printAverage()
